module.exports ={
    '@tags':['digital'],

    'digitalegy'(browser){

        
            let digital = browser.page.digitalegypt();

            digital
            .navigate()
           
            .click('@linces')
            .click('@wrong')
            .click('@startService')
            .click('@registration')
            .setValue('@nationalId','')
            .setValue('@nationalIdFactoryCode','')
            .setValue('@motherName','ا')
            .click("@nextButton")
           
            //.assert.value("//h1[text()='خدمات التموين']", "خدمات التموين")
            .saveScreenshot('tests_output/masr.png');
            
    }
}