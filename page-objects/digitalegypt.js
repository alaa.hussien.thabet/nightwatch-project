module.exports ={
    url :'https://digital.gov.eg',

    elements :
    {
        linces : {
            locateStrategy: 'xpath',
            selector:"//h5[text()='رخصى']"
        },
        wrong : 
        {
            locateStrategy: 'xpath',
            selector :"//h2[text()='استعلام عن مخالفات رخص القيادة']"
        },
        startService : {
        
            locateStrategy: 'xpath',
            selector : "//span[text()='ابدأ الخدمة']"
        },
        registration : {
            locateStrategy: 'xpath',

            selector : "//span[text()='سجّل حسابًا جديدًا']"
        },
        nationalId : {

            selector : "input[name=nationalId]"
        },
        nationalIdFactoryCode :{
            selector : "input[name=nationalIdFactoryCode]"
        },
        motherName :{
            selector : "input[name=motherName]"

        },
        nextButton :{
            locateStrategy: 'xpath',
            selector : "//span[text()='التالى']"
        },
        asert :{
            selector : "css selector",
            selector:'input[placeholder="رقم المحمول"]'
        }
    }
}
